package JobAssignment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class JobAssignment {
	public static ArrayList<Integer> list = new ArrayList<Integer>();
	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		// create item

		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);

		System.out.println("List item : " + list);
		System.out.println("Please input your number what to do(1: add, 2: view, 3: edit, 4: delete) : ");
		int number = input.nextInt();

		switch (number) {
		case 1:
			System.out.println("Add...");
			System.out.println("Please input your ID to add : ");
			int idToAdd = input.nextInt();
			addItem(idToAdd);
			break;
		
		case 2:
			System.out.println("View...");
			// System.out.println("Please input your ID to add : ");
			// int idToView = input.nextInt();
			viewItem();
			break;

		case 3:
			System.out.println("Edit...");
			System.out.println("Please input your ID to edit : ");
			int idToEdit = input.nextInt();
			System.out.println("Please input your new ID to edit : ");
			int idToEdit2 = input.nextInt();
			editItem(idToEdit, idToEdit2);
			break;

		case 4:
			System.out.println("Delete...");
			System.out.println("Please input your ID to delete : ");
			int idToDelete = input.nextInt();
			deleteItem(idToDelete);
			break;

		default:
			System.out.println("Sorry, the number you entered is incorrect. ");

		}

	}

	public static void addItem(int idToAdd1) {

		list.add(idToAdd1);

		System.out.println("ID item : " + idToAdd1 + " to add.");
		System.out.println("List item : " + list);
	}

	public static void viewItem() {
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			System.out.println(integer);
		}
	}

	public static void editItem(int idToEdit, int idToEdit2) {
		list.set(idToEdit - 1, idToEdit2);

		System.out.println("ID item : " + idToEdit + " to edit is " + idToEdit2);
		System.out.println("List item : " + list);
	}

	public static void deleteItem(int idToDelete) {
		list.remove(idToDelete - 1);

		System.out.println("ID item : " + idToDelete + " to deleted.");
		System.out.println("List item : " + list);
	}
}

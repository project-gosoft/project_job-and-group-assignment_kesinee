# Project library system
   ระบบห้องสมุดในมหาวิทยาลัยแห่งหนึ่ง

## รายละเอียด project มีดังนี้

**สิ่งที่ต้องส่ง** 

   1. Use case diagram
   2. Use case description 
   3. Class diagram
   4. Source Code  
       * เพิ่ม ลด แก้ไข ช้อมูลหนังสือได้ 
       * เพิ่ม ลด แก้ไข ผู้ใช้งานได้
       * ผู้ใช้งานสามารถ ยืม คืน หนังสือได้

**ประเภทของ user**

   1. Student
   2. Teacher
   3. Officer
   4. Library staff
   
โดย user ทั่วไปนอกจาก library staff จะสามารถทำการ ยืม/คืน หนังสือได้เพียงอย่างเดียวเท่านั้น
## Run project

ใช้การรันที่ไฟล์ main.java เพื่อเริ่มการทำงานของโปรแกรม โดยให้ user ทำการลงชื่อเข้าใช้เพื่อจำแนกประเภทของ user ที่เข้ามาใช้โปรแกรม

**รายชื่อ user ที่ได้เพิ่มไว้เพื่อใช้ทดสอบโปรแกรม มีดังนี้**

idMember  | Name | 
----- | ----- |
S201 | Kesinee | 
T101 | Chanicha |
F301 | Achiraya |
F302 | Thanachot |
L401 | Jirawat |

*idMember ตัวอักษรที่ขึ้นต้นหน้า id *
   * S : ย่อจาก Student
   * T : ย่อจาก Teacher
   * F : ย่อจาก Officer
   * L : ย่อจาก Library staff
